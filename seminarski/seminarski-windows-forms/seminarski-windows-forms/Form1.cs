﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace seminarski_windows_forms
{
    public partial class Form1 : MainForm
    {
        SqlConnection con = new SqlConnection(Helpers.GetConnectionString());
        SqlCommand cmd;
        SqlDataAdapter adapt;

        private int userId;

        public Form1(int id)
        {
            userId = id;
            var main = new MainClass();
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            _init();
            // TODO: This line of code loads data into the 'sastanciDataSet1.Sastanci' table. You can move, or remove it, as needed.
           // this.sastanciTableAdapter.Fill(this.sastanciDataSet1.Sastanci);
            // TODO: This line of code loads data into the 'napomeneDataSet1.Napomene' table. You can move, or remove it, as needed.
           // this.napomeneTableAdapter.Fill(this.napomeneDataSet1.Napomene);
            // TODO: This line of code loads data into the 'biljeskeDataSet.Biljeske' table. You can move, or remove it, as needed.
           // this.biljeskeTableAdapter.Fill(this.biljeskeDataSet.Biljeske);

        }

        private void _init()
        {
            ucitajBiljeske();
            ucitajNapomene();
        }

        private void ucitajNapomene()
        {
            try
            {
                con.Open();
                DataTable dt = new DataTable();
                adapt = new SqlDataAdapter("SELECT * FROM Napomene WHERE userId = " + userId, con);
                adapt.Fill(dt);
                dataGridView2.DataSource = dt;
                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ucitajBiljeske()
        {
            try
            {
            con.Open();
            DataTable dt = new DataTable();
            adapt = new SqlDataAdapter("SELECT * FROM KorisnikBiljeske WHERE userId = " + userId, con);
            adapt.Fill(dt);
            dataGridView1.DataSource = dt;
            con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void fillByToolStripButton_Click(object sender, EventArgs e)
        {
            try
            {
                ucitajBiljeske();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            var frm = new Biljeske(userId);
            frm.Location = this.Location;
            frm.StartPosition = FormStartPosition.Manual;
            frm.FormClosing += delegate { this.Show(); };
            frm.Show();
            this.Hide();
            ucitajBiljeske();
        }

        private void BiljeskeForma_FormClosing(object sender, FormClosingEventArgs e)
        {
            ucitajBiljeske();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            var about = new aboutBox();

            about.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var frm = new Napomene(userId);
            frm.Location = this.Location;
            frm.StartPosition = FormStartPosition.Manual;
            frm.FormClosing += delegate { this.Show(); };
            frm.Show();
            this.Hide();
            ucitajNapomene();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var sastanci = new Sastanci();

            sastanci.Show();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            _init();
        }
    }
}
