﻿namespace seminarski_windows_forms
{
    class Helpers
    {
        public static string GetConnectionString()
        {
            string dbname = "biljeske";

            if (string.IsNullOrEmpty(dbname)) return null;

            string username = "biljeskeuser";
            string password = "123456";
            string hostname = "localhost";
            string port = "1433";
// "Data Source=biljeske.cmw0gcbmsiiw.us-east-1.rds.amazonaws.com;Initial Catalog=Biljeske;User ID=biljeske;Password=123noname;"            
            return "Data Source=" + hostname + ";Initial Catalog=" + dbname + ";User ID=" + username + ";Password=" + password + ";";
        }
    }
}
