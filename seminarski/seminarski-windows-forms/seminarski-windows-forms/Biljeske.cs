﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace seminarski_windows_forms
{
    public partial class Biljeske : Form
    {
        SqlConnection con = new SqlConnection(Helpers.GetConnectionString());
        SqlCommand cmd;
        SqlDataAdapter adapt;

        private int userId;
        public Biljeske(int id)
        {
            userId = id;
            InitializeComponent();
            ucitajBiljeske();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ucitajBiljeske()
        {
            con.Open();
            DataTable dt = new DataTable();
            adapt = new SqlDataAdapter("SELECT * FROM KorisnikBiljeske WHERE userId = " + userId, con);
            adapt.Fill(dt);
            dataGridView1.DataSource = dt;
            con.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (txtNaziv.Text != "" || txtOpis.Text != "")
            {
                if (button2.Text == "Sacuvaj")
                {
                    try
                    {
                        cmd = new SqlCommand("INSERT INTO KorisnikBiljeske (naziv, opis, datum, userId) values(@naziv, @opis, @datum, @userId)", con);
                        con.Open();
                        cmd.Parameters.AddWithValue("@naziv", txtNaziv.Text);
                        cmd.Parameters.AddWithValue("@opis", txtOpis.Text);
                        cmd.Parameters.AddWithValue("@datum", dtDatum.Value);
                        cmd.Parameters.AddWithValue("@userId", userId);
                        cmd.ExecuteNonQuery();
                        con.Close();
                        lblStatus.Text = "Biljeska je dodana.";
                        reloadTable();
                    }
                    catch (System.Exception ex)
                    {
                        lblStatus.Text = "Desila se greska!";
                        System.Windows.Forms.MessageBox.Show(ex.Message);
                    }
                } else if (button2.Text == "Izmjeni")
                {
                    try
                    {
                        cmd = new SqlCommand("UPDATE Biljeske SET naziv = @naziv, opis = @opis, datum = @datum WHERE id = @id;", con);
                        con.Open();
                        cmd.Parameters.AddWithValue("@naziv", txtNaziv.Text);
                        cmd.Parameters.AddWithValue("@opis", txtOpis.Text);
                        cmd.Parameters.AddWithValue("@datum", dtDatum.Value);
                        cmd.Parameters.AddWithValue("@id", dataGridView1.CurrentRow.Cells[0].Value.ToString());
                        cmd.ExecuteNonQuery();
                        con.Close();
                        lblStatus.Text = "Biljeska je izmjenjena.";
                        reloadTable();
                    }
                    catch (System.Exception ex)
                    {
                        lblStatus.Text = "Desila se greska!";
                        System.Windows.Forms.MessageBox.Show(ex.Message);
                        con.Close();
                    }
                }
                
            } else
            {
                lblStatus.Text = "Polja ne smiju biti prazna.";
            }
            
        }

        private void Biljeske_Load(object sender, EventArgs e)
        {
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            txtNaziv.Text = txtOpis.Text = "";
            dtDatum.ResetText();
            button2.Text = "Sacuvaj";
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView1.CurrentRow.Index != 1)
            {
                txtNaziv.Text = dataGridView1.CurrentRow.Cells[1].Value.ToString();
                txtOpis.Text = dataGridView1.CurrentRow.Cells[2].Value.ToString();
                dtDatum.Value = DateTime.Parse(dataGridView1.CurrentRow.Cells[3].Value.ToString());
                button2.Text = "Izmjeni";
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (dataGridView1.CurrentRow.Index != 1)
            {
                try
                {
                    cmd = new SqlCommand("DELETE FROM Biljeske WHERE id = @id;", con);
                    con.Open();
                    cmd.Parameters.AddWithValue("@id", dataGridView1.CurrentRow.Cells[0].Value.ToString());
                    cmd.ExecuteNonQuery();
                    con.Close();
                    lblStatus.Text = "Biljeska je obrisana.";
                    reloadTable();
                }
                catch (System.Exception ex)
                {
                    lblStatus.Text = "Desila se greska!";
                    System.Windows.Forms.MessageBox.Show(ex.Message);
                    con.Close();
                }
            }
        }

        private void reloadTable()
        {
            ucitajBiljeske();
        }
    }
}
