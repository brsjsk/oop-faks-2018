﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace seminarski_windows_forms
{
    public partial class Napomene : Form
    {
        SqlConnection con = new SqlConnection(Helpers.GetConnectionString());
        SqlCommand cmd;
        SqlDataAdapter adapt;

        private int userId;

        public Napomene(int id)
        {
            userId = id;
            InitializeComponent();
            ucitajNapomene();
        }

        private void ucitajNapomene()
        {
            try
            {
                con.Open();
                DataTable dt = new DataTable();
                adapt = new SqlDataAdapter("SELECT * FROM Napomene WHERE userId = " + userId, con);
                adapt.Fill(dt);
                dataGridView1.DataSource = dt;
                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (button1.Text == "Sacuvaj")
            {
            cmd = new SqlCommand("INSERT INTO Napomene (opis, rok, userId) values(@opis, @rok, @userId)", con);
            con.Open();
            cmd.Parameters.AddWithValue("@opis", textBox1.Text);
            cmd.Parameters.AddWithValue("@rok", dateTimePicker1.Value);
            cmd.Parameters.AddWithValue("@userId", userId);
            cmd.ExecuteNonQuery();
            con.Close();
            lblStatus.Text = "Napomena je dodana.";
            ucitajNapomene();
            } else
            {
                cmd = new SqlCommand("UPDATE Napomene SET opis = @opis, rok = @rok WHERE id = @id;", con);
                con.Open();
                cmd.Parameters.AddWithValue("@opis", textBox1.Text);
                cmd.Parameters.AddWithValue("@rok", dateTimePicker1.Value);
                cmd.Parameters.AddWithValue("@id", dataGridView1.CurrentRow.Cells[0].Value.ToString());
                cmd.ExecuteNonQuery();
                con.Close();
                lblStatus.Text = "Napomena je izmjenjena.";
                ucitajNapomene();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            dateTimePicker1.ResetText();
            button1.Text = "Sacuvaj";
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView1.CurrentRow.Index != 1)
            {
                textBox1.Text = dataGridView1.CurrentRow.Cells[1].Value.ToString();
                dateTimePicker1.Value = DateTime.Parse(dataGridView1.CurrentRow.Cells[2].Value.ToString());
                button1.Text = "Izmjeni";
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (dataGridView1.CurrentRow.Index != 1)
            {
                try
                {
                    cmd = new SqlCommand("DELETE FROM Napomene WHERE id = @id;", con);
                    con.Open();
                    cmd.Parameters.AddWithValue("@id", dataGridView1.CurrentRow.Cells[0].Value.ToString());
                    cmd.ExecuteNonQuery();
                    con.Close();
                    lblStatus.Text = "Napomena je obrisana.";
                    ucitajNapomene();
                }
                catch (System.Exception ex)
                {
                    lblStatus.Text = "Desila se greska!";
                    System.Windows.Forms.MessageBox.Show(ex.Message);
                    con.Close();
                }
            }
        }
    }
}
