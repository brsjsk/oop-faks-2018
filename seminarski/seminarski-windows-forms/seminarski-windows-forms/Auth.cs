﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace seminarski_windows_forms
{
    public partial class Auth : MainForm
    {
        SqlConnection con = new SqlConnection(Helpers.GetConnectionString());
        SqlCommand cmd;

        public Auth()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            pctLoading.Visible = true;

            cmd = new SqlCommand("SELECT * FROM Korisnici WHERE email = @email AND password = @password", con);
            con.Open();
            cmd.Parameters.AddWithValue("@email", txtEmailLogin.Text);
            cmd.Parameters.AddWithValue("@password", txtPasswordLogin.Text);
            var dbr = cmd.ExecuteReader();
            int count = 0;
            int userId = 0;
            while (dbr.Read())
            {
                count = count + 1;
                userId = dbr.GetInt32(0);
            }
            if (count == 1)
            {
               // string employeeID = dbr.GetString(3);
                con.Close();
                var frm = new Form1(userId);
                frm.Location = this.Location;
                frm.StartPosition = FormStartPosition.Manual;
                frm.FormClosing += delegate { this.Show(); };
                pctLoading.Visible = false;
                frm.Show();
                this.Hide();
            }
            else
            {
                lblHintLog.Text = "Provjerite korisnicko ime ili sifru!";
                pctLoading.Visible = false;
                con.Close();
            }
          }

        private void btnReg_Click(object sender, EventArgs e)
        {
            if (txtImeReg.Text == "" || txtPrezimeReg.Text == "" || txtEmailReg.Text == "" || txtPasswordReg.Text == "")
            {
                lblHintReg.Text = "Popunite sva polja";
            } else
            {
                cmd = new SqlCommand("SELECT * FROM Korisnici WHERE email = @email", con);
                con.Open();
                cmd.Parameters.AddWithValue("@email", txtEmailReg.Text);
                var dbr = cmd.ExecuteReader();
                int count = 0;

                while (dbr.Read())
                {
                    count = count + 1;
                }
                if (count == 1)
                {
                    lblHintReg.Text = "Email je zauzet!";
                    con.Close();
                }
                else
                {
                    con.Close();
                    cmd = new SqlCommand("INSERT INTO Korisnici (ime, prezime, email, password) values(@ime, @prezime, @email, @password)", con);
                    con.Open();
                    cmd.Parameters.AddWithValue("@ime", txtImeReg.Text);
                    cmd.Parameters.AddWithValue("@prezime", txtPrezimeReg.Text);
                    cmd.Parameters.AddWithValue("@email", txtEmailReg.Text);
                    cmd.Parameters.AddWithValue("@password", txtPasswordReg.Text);
                    cmd.ExecuteNonQuery();
                    con.Close();
                    lblHintReg.Text = "Uspjesno ste se registrovali.";
                }

                
            }
        }
    }
}
